public class MethodsTest
{
    public static void main(String[]args)
    {
        int x = 5;
        System.out.println(x);
        methodNoInputNoReturn();
        System.out.println(x);
        System.out.println(x);
        methodOneInputNoReturn(x + 10);
        System.out.println(x);
        methodTwoInputNoReturn(10, 3.5);
        int z = methodNoInputReturnInt();
        System.out.println(z);
        double squareRoot = sumSquareRoot(9, 5);
        System.out.println(squareRoot);
        String s1 = "java";
        String s2 = "programming";
        System.out.println(s1.length());
        System.out.println(s2.length());
        int addOne = SecondClass.addOne(50);
        System.out.println(addOne);
        SecondClass sc = new SecondClass();
        int addTwo = sc.addTwo(50);
        System.out.println(addTwo);
    }
    public static void methodNoInputNoReturn()
    {
        System.out.println("I'm in a method that takes no input and returns nothing");
        int x = 20;
        System.out.println(x);
    }
    public static void methodOneInputNoReturn(int y)
    {
        y-=5;
        System.out.println("Inside the method one input no return");
        System.out.println(y);
    }
    public static void methodTwoInputNoReturn(int x, double y)
    {
        System.out.println(x);
        System.out.println(y);
    }
    public static int methodNoInputReturnInt()
    {
        System.out.println("methodNoInputReturnInt");
        return 5;
    }
    public static double sumSquareRoot(int x, int y)
    {
        int z = x + y;
        return Math.sqrt(z);
    }
}