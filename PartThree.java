import java.util.Scanner;
public class PartThree
{
    public static void main(String[]args)
    {
        Calculator c = new Calculator();
        Scanner scan = new Scanner(System.in);
        System.out.println("Input the first number");
        double userInput1 = scan.nextDouble();
        System.out.println("Input the second number");
        double userInput2 = scan.nextDouble();
        double add = Calculator.add(userInput1, userInput2);
        double sub = Calculator.subtract(userInput1, userInput2);
        double mul = c.multiply(userInput1, userInput2);
        double div = c.divide(userInput1, userInput2);
        System.out.println("Addition = " + add + " substract = " + sub + " Multiplication = " + mul + " Division = " + div);
    }
}